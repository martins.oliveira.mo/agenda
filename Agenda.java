import java.util.*;
public class Agenda
{


    public static void main (String[] args)
    {

        Scanner lerTeclado = new Scanner(System.in);
        ArrayList<Contato> listaContatos = new ArrayList<Contato>();
        int opcao = -1;

        while(opcao!=0)
        {
            System.out.println("1- para novo contato");
            System.out.println("2- para listar um contato");
            System.out.println("3- para remover um contato");
            int casos = lerTeclado.nextInt();

            switch(casos)
            {	
		case 1:
                Contato umContato=new Contato();
                System.out.println("Digite o nome do contato:");
                String umNome = lerTeclado.nextLine();
                umContato.setNome(umNome);

                System.out.println("Digite o numéro do telefone:");
                String umTelefone= lerTeclado.nextLine();
                umContato.setTelefone(umTelefone);

                System.out.println("Qual o sexo:");
                int umSexo= lerTeclado.nextInt();
                umContato.setSexo(umSexo);

                listaContatos.add(umContato);

                System.out.println("Contato inserido com sucesso :)");
                System.out.println("Nome: "+umContato.getNome());
                System.out.println("Telefone: "+umContato.getTelefone());
                if (umContato.getSexo()==0)
                {
                    System.out.println("Sexo: Masculino");
                }
                else if (umContato.getSexo()==1)
                {
                    System.out.println("Sexo: Feminino");
                }
                else
                {
                    System.out.println("Sexo: não especificado");
                }

                break;

            case 2:
                int tamanhoArray=listaContatos.size();
                int icont;
                for(icont=0; icont<tamanhoArray; icont++)
                {
                    System.out.println("Contato: "+(icont-1));
                    System.out.println("Nome: "+listaContatos.get(icont).getNome());
                    System.out.println("Número: "+listaContatos.get(icont).getTelefone());
                    System.out.println("Sexo: "+listaContatos.get(icont).getSexo());
                }
                break;

            case 3:
                System.out.println("Qual contato deseja remover(de acordo com idice de contato)");
                int excluir=lerTeclado.nextInt();
                lerTeclado.nextLine();
                listaContatos.remove(excluir-1);

                break;
            }

        }
    }
}
